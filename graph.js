var graph = require('@microsoft/microsoft-graph-client');

module.exports = {
  getUserDetails: async function(accessToken) {
    const client = getAuthenticatedClient(accessToken);

    const user = await client.api('').get();
    return user;
  },

  getEvents: async function(accessToken, today, tomorrow) {
    const client = getAuthenticatedClient(accessToken);

    const events = await client
      .api('me/calendars/AAMkAGFhMzljZmQ4LTcwODgtNDcxYS05NTlhLWQwZGNhNDExMmI0ZgBGAAAAAAD2rZWDmmfzQaRJlxmT8eyGBwCj1dnzbu0QRbX3bkIh15fTAAAAAAEGAACj1dnzbu0QRbX3bkIh15fTAAAAAB0HAAA=/calendarview?startdatetime=' + today + '&enddatetime=' + tomorrow + '&orderby=start/DateTime')
      .header('Prefer', 'outlook.timezone="Central Standard Time"')
      .select('subject,organizer,start,end,isCancelled, isAllDay')
      .get();
    console.log(events);
    return events;
  }
};

function getAuthenticatedClient(accessToken) {
  // Initialize Graph client
  const client = graph.Client.init({
    // Use the provided access token to authenticate
    // requests
    authProvider: (done) => {
      done(null, accessToken);
    }
  });

  return client;
}