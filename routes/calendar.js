var express = require('express');
var router = express.Router();
var tokens = require('../tokens.js');
var graph = require('../graph.js');



/* GET /calendar */
router.get('/',
  async function(req, res) {

    if (!req.isAuthenticated()) {
      // Redirect unauthenticated requests to home page
      res.redirect('/')
    } else {
      let params = {
        active: { calendar: true }
      };

      // Get the access token
      var accessToken;
      try {
        accessToken = await tokens.getAccessToken(req);
      } catch (err) {
        req.flash('error_msg', {
          message: 'Could not get access token. Try signing out and signing in again.',
          debug: JSON.stringify(err)
        });
      }

      if (accessToken && accessToken.length > 0) {
        try {
          //create today and tomorrow objects to append onto API call, 
          //so it only ever returns events from today-tomorrow
          var moment = require('moment');
          var today = new Date();
          
          today = moment(today).format('YYYY-MM-DD');
          tomorrow = moment(today).add(1, 'days').format('YYYY-MM-DD');

           // Get the events
          var events = await graph.getEvents(accessToken, today, tomorrow);
          params.events = events.value;
        } catch (err) {
          req.flash('error_msg', {
            message: 'Could not fetch events',
            debug: JSON.stringify(err)
          });
        }
      }

      res.render('calendar', params);
    }
  }
);

module.exports = router;